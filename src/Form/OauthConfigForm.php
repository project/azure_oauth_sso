<?php

namespace Drupal\azure_oauth_sso\Form;

use Drupal\azure_oauth_sso\BaseOAuth;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides base OauthConfigForm functionality.
 */
class OauthConfigForm extends ConfigFormBase {
  use BaseOAuth;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new BaseOAuth object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, RequestStack $request_stack) {
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Retrieves the names of the editable configuration.
   */
  protected function getEditableConfigNames() {
    return ['azure_oauth_sso_config.settings'];
  }

  /**
   * Builds the form.
   */
  public function getFormId() {
    return 'azure_oauth_sso_config_form';
  }

  /**
   * Builds the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('azure_oauth_sso_config.settings');

    $form['client_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required'      => TRUE,
      '#description'   => $this->t('Get the Client ID from the application overview page.'),
    ];

    $form['ad_tenant'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Tenant ID'),
      '#default_value' => $config->get('ad_tenant'),
      '#placeholder'   => '123e4567-e89b-12d3-a456-426614174000',
      '#required'      => TRUE,
      '#description'   => $this->t('Get the Tenant ID from the application overview page.'),
    ];

    $form['client_secret'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Client Secret Value'),
      '#default_value' => $config->get('client_secret'),
      '#required'      => TRUE,
      '#description'   => $this->t('Get the value of the Client Secret when you create a new client secret immediately.'),
    ];

    $form['redirect_uri'] = [
      '#markup' => '<div>
                <div class="container-inline"><b>' . $this->t('Redirect URI:') . '</b>
                <div id="idp_metadata_url">
                    <code><b><span>' . $this->getRedirectUri() . '</span></b></code>
                </div>
                <span class ="mo_copy button button--small">&#128461; ' . $this->t('Copy') . '</span>
                </div>
            </div>',
    ];

    $form['options_login'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Options for login page:'),
      '#required'      => TRUE,
      '#default_value' => $config->get('options_login'),
      '#options'       => [
        'redirect'   => $this->t('Immediate Redirect'),
        'login_form' => $this->t('Show microsoft login button on login form'),
      ],
    ];

    $form['enable_custom_form_ids'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Custom Form IDs'),
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
      '#default_value' => $config->get('enable_custom_form_ids'),
      '#states'        => [
        'visible' => [
          ':input[name="options_login"]' => ['value' => 'login_form'],
        ],
      ],
    ];

    $form['login_link_text'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Button Text'),
      '#default_value' => $config->get('login_link_text'),
      '#states'        => [
        'visible' => [
          ':input[name="options_login"]' => ['value' => 'login_form'],
        ],
      ],
    ];

    $form['custom_form_ids'] = [
      '#type'          => 'textarea',
      '#rows'          => 1,
      '#default_value' => $config->get('enable_custom_form_ids') ? $config->get('custom_form_ids') : "",
      '#attributes'    => ['placeholder' => $this->t('Enter form IDs separated by commas, for example: user_form, login_form.')],
      '#description'   => $this->t('Enter the form IDs on which you want to display SSO link.'),
      '#states'        => [
        'visible' => [
          ':input[name="enable_custom_form_ids"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['enable_sso_logout'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable SSO Logout'),
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
      '#default_value' => $config->get('enable_sso_logout'),
    ];

    $params = [];
    $url = Url::fromRoute('azure_oauth_sso.testOauthLogin', $params);
    $url_string = $url->toString();

    $form['test_config_button'] = [
      '#type'       => 'link',
      '#title'      => $this->t('Test Configuration'),
      '#url'        => $url,
      '#attributes' => [
        'class'   => ['button', 'button--small'],
        'onclick' => 'testconfig("' . $url_string . '"); return false;',
      ],
    ];

    $form['#attached']['library'][] = 'azure_oauth_sso/basic-config';
    return parent::buildForm($form, $form_state);
  }

  /**
   * Handles form submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('azure_oauth_sso_config.settings');

    $config->set('client_id', $form_state->getValue('client_id'));
    $config->set('ad_tenant', $form_state->getValue('ad_tenant'));
    $config->set('client_secret', $form_state->getValue('client_secret'));
    $config->set('redirect_uri', $form_state->getValue('redirect_uri'));
    $config->set('options_login', $form_state->getValue('options_login'));
    $config->set('login_link_text', $form_state->getValue('login_link_text'));
    $config->set('enable_sso_logout', $form_state->getValue('enable_sso_logout'));
    $config->set('enable_custom_form_ids', $form_state->getValue('enable_custom_form_ids'));
    $config->set('custom_form_ids', $form_state->getValue('custom_form_ids'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
