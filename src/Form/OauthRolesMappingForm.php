<?php

namespace Drupal\azure_oauth_sso\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides base OauthRolesMappingForm functionality.
 */
class OauthRolesMappingForm extends ConfigFormBase {

  /**
   * Retrieves the names of the editable configuration.
   */
  protected function getEditableConfigNames() {
    return ['azure_oauth_sso_roles_mapping.settings'];
  }

  /**
   * Builds the form.
   */
  public function getFormId() {
    return 'azure_oauth_sso_roles_mapping_form';
  }

  /**
   * Builds the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('azure_oauth_sso_roles_mapping.settings');

    $roles = user_roles(TRUE);

    $role_options = [];
    foreach ($roles as $role) {
      $role_options[$role->id()] = $role->label();
    }

    $form['default_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Role'),
      '#options' => $role_options,
      '#description' => $this->t('Choose a role for the user.'),
      '#required' => TRUE,
      '#default_value' => $config->get('default_role'),
    ];

    $form['role_options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mapping for'),
      '#id' => 'role-options-mapping',
      '#options' => [
        'ad_group' => $this->t('Microsoft Group'),
        'department_field' => $this->t('Microsoft Department Field'),
      ],
      '#default_value' => $config->get('role_options') ?? NULL,
      '#description' => $this->t("* If the (AD Group) option is chosen, you must enter the group Opject ID from AD.<br>* If the (Department Field Property) is chosen, you must enter the value of the department field from AD."),
    ];

    $form['custom_attribute_mapping']['custom_attr_table'] = [
      '#type' => 'table',
      '#id' => 'attribute-mapping',
      '#header' => [$this->t('Drupal Roles')],
      '#empty' => $this->t('No custom attributes found'),
      '#states' => [
        'visible' => [
          ':input[name="enable_role_mapping"]' => ['checked' => TRUE],
        ],
      ],
    ];

    foreach ($role_options as $role_key => $role_label) {
      $form['custom_attribute_mapping']['custom_attr_table'][$role_key] = [
        '#states' => [
          'visible' => [':input[name="enable_role_mapping"]' => ['checked' => TRUE]],
        ],
        'drupal_attribute' => [
          'data' => [
            '#type' => 'checkbox',
            '#title' => $this->t('@label', ['@label' => $role_label]),
            '#attributes' => [
              'class' => ['custom-attribute-checkbox'],
            ],
          ],
        ],
        'textfield' => [
          'data' => [
            '#type' => 'textfield',
            '#default_value' => $config->get("custom_attr_table.$role_key", ''),
            '#attributes' => [
              'disabled' => !$config->get("custom_attr_table.$role_key"),
            ],
          ],
        ],
      ];
    }

    $form['custom_attribute_mapping']['custom_attr_table']['#multiple'] = TRUE;
    $form['custom_attribute_mapping']['custom_attr_table']['#empty'] = $this->t('No custom attributes found');

    $form['#attached'] = [
      'library' => [
        'azure_oauth_sso/roles-mapping',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Handles form submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('azure_oauth_sso_roles_mapping.settings');
    $config->set('default_role', $form_state->getValue('default_role'));
    $config->set('role_options', $form_state->getValue('role_options'));

    $customAttrTable = $form_state->getValue('custom_attr_table');

    foreach ($customAttrTable as $roleKey => $values) {
      if (!empty($values['drupal_attribute'])) {
        $config->set("custom_attr_table.$roleKey", $values['textfield']['data']);
      }
    }

    $config->save();

  }

}
