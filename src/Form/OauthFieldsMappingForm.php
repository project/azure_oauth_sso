<?php

namespace Drupal\azure_oauth_sso\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base OauthConfigForm functionality.
 */
class OauthFieldsMappingForm extends ConfigFormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $configFactory) {
    $this->entityFieldManager = $entity_field_manager;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Retrieves the names of the editable configuration.
   */
  protected function getEditableConfigNames() {
    return ["azure_oauth_sso_fields_mapping.settings"];
  }

  /**
   * Builds the form.
   */
  public function getFormId() {
    return "azure_oauth_sso_fields_mapping_form";
  }

  /**
   * Builds the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config("azure_oauth_sso_fields_mapping.settings");

    $entity_field_manager = $this->entityFieldManager;
    $entities = $entity_field_manager->getFieldDefinitions("user", "user");
    $checkbox_options = [];

    foreach ($entities as $key => $value) {
      if (
        !in_array($key, [
          "roles",
          "user_picture",
          "pass",
          "field_refresh_token",
          "timezone",
          "field_access_token",
          "uid",
          "uuid",
          "preferred_admin_langcode",
          "preferred_langcode",
          "langcode",
          "created",
          "changed",
          "access",
          "created",
          "login",
          "init",
          "default_langcode",
          "metatag",
          "path",
          "field_last_password_reset",
          "field_password_expiration",
          "field_pending_expire_sent",
          "status",
        ])
      ) {
        $checkbox_options[$key] = $key;
      }
    }

    $form["custom_attribute_mapping"]["custom_attr_table"] = [
      "#type" => "table",
      "#id" => "attribute-mapping",
      "#header" => [$this->t("Drupal Fields"), $this->t("AD Property")],
      "#empty" => $this->t("No custom attributes found"),
    ];

    foreach ($checkbox_options as $checkbox_key => $checkbox_label) {
      $form["custom_attribute_mapping"]["custom_attr_table"][$checkbox_key] = [
        "drupal_attribute" => [
          "data" => [
            "#type" => "checkbox",
            "#title" => $this->t("@label", ["@label" => $checkbox_label]),
            "#attributes" => [
              "class" => ["custom-attribute-checkbox"],
            ],
          ],
        ],
        "ad_property" => [
          "data" => [
            "#type" => "select",
            "#options" => [
              "_none" => "Please Select",
              "aboutMe" => "aboutMe",
              "accountEnabled" => "accountEnabled",
              "ageGroup" => "ageGroup",
              "assignedLicenses" => "assignedLicenses",
              "assignedPlans" => "assignedPlans",
              "birthday" => "birthday",
              "businessPhones" => "businessPhones",
              "city" => "city",
              "companyName" => "companyName",
              "consentProvidedForMinor" =>
              "consentProvidedForMinor",
              "country" => "country",
              "createdDateTime" => "createdDateTime",
              "creationType" => "creationType",
              "customSecurityAttributes" =>
              "customSecurityAttributes",
              "deletedDateTime" => "deletedDateTime",
              "department" => "department",
              "displayName" => "displayName",
              "employeeHireDate" => "employeeHireDate",
              "employeeLeaveDateTime" => "employeeLeaveDateTime",
              "employeeId" => "employeeId",
              "employeeOrgData" => "employeeOrgData",
              "employeeType" => "employeeType",
              "externalUserState" => "externalUserState",
              "externalUserStateChangeDateTime" =>
              "externalUserStateChangeDateTime",
              "givenName" => "givenName",
              "hireDate" => "hireDate",
              "id" => "id",
              "identities" => "identities",
              "imAddresses" => "imAddresses",
              "interests" => "interests",
              "isResourceAccount" => "isResourceAccount",
              "jobTitle" => "jobTitle",
              "lastPasswordChangeDateTime" =>
              "lastPasswordChangeDateTime",
              "legalAgeGroupClassification" =>
              "legalAgeGroupClassification",
              "licenseAssignmentStates" =>
              "licenseAssignmentStates",
              "mail" => "mail",
              "mailboxSettings" => "mailboxSettings",
              "mailNickname" => "mailNickname",
              "mobilePhone" => "mobilePhone",
              "mySite" => "mySite",
              "officeLocation" => "officeLocation",
              "onPremisesDistinguishedName" =>
              "onPremisesDistinguishedName",
              "onPremisesDomainName" => "onPremisesDomainName",
              "onPremisesExtensionAttributes" =>
              "onPremisesExtensionAttributes",
              "onPremisesImmutableId" => "onPremisesImmutableId",
              "onPremisesLastSyncDateTime" =>
              "onPremisesLastSyncDateTime",
              "onPremisesProvisioningErrors" =>
              "onPremisesProvisioningErrors",
              "onPremisesSamAccountName" =>
              "onPremisesSamAccountName",
              "onPremisesSecurityIdentifier" =>
              "onPremisesSecurityIdentifier",
              "onPremisesSyncEnabled" => "onPremisesSyncEnabled",
              "onPremisesUserPrincipalName" =>
              "onPremisesUserPrincipalName",
              "otherMails" => "otherMails",
              "passwordPolicies" => "passwordPolicies",
              "passwordProfile" => "passwordProfile",
              "pastProjects" => "pastProjects",
              "postalCode" => "postalCode",
              "preferredLanguage" => "preferredLanguage",
              "preferredName" => "preferredName",
              "provisionedPlans" => "provisionedPlans",
              "proxyAddresses" => "proxyAddresses",
              "refreshTokensValidFromDateTime" =>
              "refreshTokensValidFromDateTime",
              "responsibilities" => "responsibilities",
              "serviceProvisioningErrors" =>
              "serviceProvisioningErrors",
              "schools" => "schools",
              "securityIdentifier" => "securityIdentifier",
              "showInAddressList" => "showInAddressList",
              "signInActivity" => "signInActivity",
              "signInSessionsValidFromDateTime" =>
              "signInSessionsValidFromDateTime",
              "skills" => "skills",
              "state" => "state",
              "streetAddress" => "streetAddress",
              "surname" => "surname",
              "usageLocation" => "usageLocation",
              "userPrincipalName" => "userPrincipalName",
              "userType" => "userType",
            ],
            "#default_value" => $config->get("custom_attr_table.$checkbox_key", "") ?? NULL,
            "#attributes" => [
              "disabled" => !$config->get("custom_attr_table.$checkbox_key") ?? NULL,
            ],
          ],
        ],
      ];
    }

    $form["user_picture"] = [
      "#type" => "checkbox",
      "#title" => $this->t("Sync profile picture."),
      "#collapsible" => TRUE,
      "#collapsed" => TRUE,
      "#default_value" => $config->get("user_picture") ?? NULL,
    ];

    $form["custom_attribute_mapping"]["custom_attr_table"]["#multiple"] = TRUE;
    $form["custom_attribute_mapping"]["custom_attr_table"]["#empty"] = $this->t("No custom attributes found");

    $form["#attached"] = ["library" => ["azure_oauth_sso/fields-mapping"]];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Handles form submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('azure_oauth_sso_fields_mapping.settings');

    $custom_attr_values = $form_state->getValue("custom_attr_table");

    foreach ($custom_attr_values as $key => $selected) {
      if ($selected["drupal_attribute"]) {
        $config->set("custom_attr_table.$key", $selected["ad_property"]["data"]);
      }
    }

    $user_picture_value = $form_state->getValue("user_picture");
    $config->set("user_picture", $user_picture_value);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
