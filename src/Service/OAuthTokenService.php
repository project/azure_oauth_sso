<?php

namespace Drupal\azure_oauth_sso\Service;

use Drupal\azure_oauth_sso\BaseOAuth;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides base OAuthTokenService functionality.
 */
class OAuthTokenService {

  use BaseOAuth;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Azure AD tenant.
   *
   * @var string
   */
  protected $adTenant;

  /**
   * Azure client ID.
   *
   * @var string
   */
  protected $clientID;

  /**
   * Azure client secret.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * Constructs a new OAuthTokenService object.
   */
  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->adTenant = $config_factory->get('azure_oauth_sso_config.settings')->get('ad_tenant');
    $this->clientID = $config_factory->get('azure_oauth_sso_config.settings')->get('client_id');
    $this->clientSecret = $config_factory->get('azure_oauth_sso_config.settings')->get('client_secret');
  }

  /**
   * Retrieves the access token.
   */
  public function getToken() {
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    return $user->get('field_refresh_token')->value;
  }

  /**
   * Refreshes the access token.
   */
  public function refreshToken() {
    $client = new Client();

    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $refreshToken = $user->get('field_refresh_token')->value;

    $res = $client->post('https://login.microsoftonline.com/' . $this->getAdTenant() . '/oauth2/v2.0/token', [
      'form_params' => [
        'client_id'     => $this->getClientId(),
        'refresh_token' => $refreshToken,
        'grant_type'    => 'refresh_token',
        'client_secret' => $this->getClientSecret(),
      ],
    ]);

    $response = json_decode($res->getBody(), TRUE);
    if (isset($response['access_token']) && isset($response['refresh_token'])) {
      $user->set("field_access_token", 'Bearer ' . $response["access_token"]);
      $user->set("field_refresh_token", $response["refresh_token"]);
      $user->save();

      return 'Bearer ' . $response["access_token"];
    }
  }

  /**
   * Makes an API call using the access token.
   */
  public function apiCall($url, $params = [], $method = 'POST') {
    $client = new Client();

    try {
      $response = $client->request($method, $url, $params);

      $contentType = $response->getHeader('Content-Type')[0];
      if (strpos($contentType, 'image') !== FALSE) {
        return $response->getBody()->getContents();
      }

      $responseData = json_decode($response->getBody()->getContents(), TRUE);
      return $responseData;
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        $statusCode = $e->getResponse()->getStatusCode();

        if ($statusCode == 401 || $statusCode == 400) {
          $params['headers']['Authorization'] = $this->refreshToken();
          return $this->apiCall($url, $params, $method);
        }
      }
    }
  }

}
