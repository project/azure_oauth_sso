<?php

namespace Drupal\azure_oauth_sso;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides base BaseOAuth functionality.
 */
trait BaseOAuth {

  /**
   * Configuration settings for the application.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Language setting for the application.
   *
   * @var string
   */
  protected $language;

  /**
   * Host URL for the application.
   *
   * @var string
   */
  protected $host;

  /**
   * Client ID for authentication.
   *
   * @var string
   */
  protected $clientID;

  /**
   * Tenant ID for the advertisement service.
   *
   * @var string
   */
  protected $adTenant;

  /**
   * Client secret for authentication.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * Redirect URI for authentication callbacks.
   *
   * @var string
   */
  protected $redirectUri;

  /**
   * Constructs a new BaseOAuth object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, RequestStack $request_stack) {
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * Retrieves the Client ID from configuration settings.
   */
  protected function getClientId() {
    if ($this->configFactory) {
      $config = $this->configFactory->get('azure_oauth_sso_config.settings');
      return $config->get('client_id');
    }
    return NULL;
  }

  /**
   * Retrieves the Ad Tenant ID from configuration settings.
   */
  public function getAdTenant() {
    if ($this->configFactory) {
      $config = $this->configFactory->get('azure_oauth_sso_config.settings');
      return $config->get('ad_tenant');
    }
    return NULL;
  }

  /**
   * Retrieves the Client Secret from configuration settings.
   */
  protected function getClientSecret() {
    if ($this->configFactory) {
      $config = $this->configFactory->get('azure_oauth_sso_config.settings');
      return $config->get('client_secret');
    }
    return NULL;
  }

  /**
   * Retrieves the Redirect URI for authentication callbacks.
   */
  protected function getRedirectUri() {
    $current_request = $this->requestStack->getCurrentRequest();
    $this->language = ($this->languageManager) ? $this->languageManager->getCurrentLanguage()->getId() : null;
    // override
    $this->language = null;
    
    $this->host = $current_request->getSchemeAndHttpHost();
    if (is_null($this->language)) {
        $this->redirectUri = $this->host . '/oauth/login';
    } else {
        $this->redirectUri = $this->host . '/' . $this->language . '/oauth/login';
    }
    return $this->redirectUri;
  }

}
