<?php

namespace Drupal\azure_oauth_sso\Controller;

use Drupal\azure_oauth_sso\BaseOAuth;
use Drupal\azure_oauth_sso\Service\OAuthTokenService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides base OauthLogin functionality.
 */
class OauthLogin extends ControllerBase implements ContainerInjectionInterface {
  use BaseOAuth;
  /**
   * The authCode.
   *
   * @var string
   */
  private $authCode;

  /**
   * The rolesMapping.
   *
   * @var string
   */
  private $rolesMapping;

  /**
   * The fieldsMapping.
   *
   * @var string
   */
  private $fieldsMapping;

  /**
   * The accessToken.
   *
   * @var string
   */
  public $accessToken;

  /**
   * The accessToken.
   *
   * @var string
   */
  public $refreshToken;

  /**
   * The displayName.
   *
   * @var string
   */
  public $displayName;

  /**
   * The department.
   *
   * @var string
   */
  public $department;

  /**
   * The configFactory.
   *
   * @var string
   */
  protected $configFactory;

  /**
   * The currentUser.
   *
   * @var string
   */
  protected $currentUser;

  /**
   * The entityTypeManager.
   *
   * @var string
   */
  protected $entityTypeManager;

  /**
   * The oauthService.
   *
   * @var string
   */
  protected $oauthService;

  /**
   * The requestStack.
   *
   * @var string
   */
  protected $requestStack;

  public function __construct(OAuthTokenService $oauthService, EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory, RequestStack $requestStack, AccountProxyInterface $currentUser) {
    $this->configFactory = $configFactory;
    $this->requestStack = $requestStack;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->oauthService = $oauthService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('azure_oauth_sso.token_service'),
        $container->get('entity_type.manager'),
        $container->get('config.factory'),
        $container->get('request_stack'),
        $container->get('current_user')
    );
  }

  /**
   * Generates a token for authentication.
   *
   *   The generated token.
   */
  private function generateToken() {
    $client = new Client();

    try {
      $res = $client->post(
        'https://login.microsoftonline.com/' . $this->getAdTenant() . '/oauth2/v2.0/token', [
          'form_params' => [
            'grant_type'    => 'authorization_code',
            'client_id'     => $this->getClientId(),
            'redirect_uri'  => $this->getRedirectUri(),
            'code'          => $this->authCode,
            'client_secret' => $this->getClientSecret(),
          ],
        ]
      );

      $response = json_decode($res->getBody(), TRUE);

      if (isset($response['access_token']) && isset($response['refresh_token'])) {
        $this->accessToken = 'Bearer ' . $response['access_token'];
        $this->refreshToken = $response["refresh_token"];
      }
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        $statusCode = $e->getResponse()->getStatusCode();

        if ($statusCode == 401 || $statusCode == 400) {
          $path = Url::fromRoute('azure_oauth_sso.oauthLogin')->toString();
          $response = new RedirectResponse($path);
          $response->send();
          exit();
        }
      }
    }
  }

  /**
   * Tests OAuth login functionality.
   */
  public function testOauthLogin() {
    $request = $this->requestStack->getCurrentRequest();

    if (!$request->query->has("code") && !$request->query->has("error")) {
      $url = "https://login.microsoftonline.com/" . $this->getAdTenant() . "/oauth2/v2.0/authorize?";
      $url .= "client_id=" . $this->getClientId();
      $url .= "&response_type=code";
      $url .= "&response_mode=query";
      $url .= "&scope=https://graph.microsoft.com/User.Read offline_access";
      $url .= "&state=12345";
      return new TrustedRedirectResponse($url);
    }
  }

  /**
   * Performs OAuth login.
   */
  public function oauthLogin() {
    $currentRequest = $this->requestStack->getCurrentRequest();
    $code = $currentRequest->query->get('code');
    $error = $currentRequest->query->get('error');

    $user = $this->currentUser;
    if ($code === NULL && $error === NULL && !$user->isAuthenticated()) {
      $url = "https://login.microsoftonline.com/" . $this->getAdTenant() . "/oauth2/v2.0/authorize?";
      $url .= "client_id=" . $this->getClientId();
      $url .= "&response_type=code";
      $url .= "&redirect_uri=" . urlencode($this->getRedirectUri());
      $url .= "&response_mode=query";
      $url .= "&scope=https://graph.microsoft.com/User.Read offline_access";
      $url .= "&state=12345";

      return new TrustedRedirectResponse($url);
    }

    if ($currentRequest->query->has('code')) {
      $this->authCode = $currentRequest->query->get('code');
      $userPicture = $this->configFactory->get('azure_oauth_sso_fields_mapping.settings')->get('user_picture');

      if (!$user->isAuthenticated()) {
        $this->generateToken();
        $this->loginUser();
        if ($userPicture == 1) {
          $this->getMyPhoto();
        }
      }
    }

    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

  /**
   * Logs a user in.
   */
  public function loginUser() {
    $apiRes = $this->getPersonalData();
    $email = $apiRes['mail'];

    $user_storage = $this->entityTypeManager->getStorage('user');
    $query = $user_storage->getQuery();
    $query->condition('mail', $email);
    $query->accessCheck(TRUE);
    $uids = $query->execute();

    $found = !empty($uids);

    if ($found) {
      $user = $user_storage->load(reset($uids));

      $fields = array_flip($this->configFactory->get('azure_oauth_sso_fields_mapping.settings')->get('custom_attr_table'));
      $fieldsKeys = array_keys($fields);
      $matchedValues = [];

      foreach ($fieldsKeys as $key) {
        if (array_key_exists($key, $apiRes)) {
          $matchedValues[$fields[$key]] = $apiRes[$key];
        }
      }

      $userValues = [];

      foreach ($matchedValues as $mappedFieldName => $fieldName) {
        if ($user->hasField($mappedFieldName)) {
          $fieldValue = $user->get($mappedFieldName)->value;
          $userValues[$mappedFieldName] = $fieldValue;
        }
      }

      $differences = array_diff_assoc($matchedValues, $userValues);

      if (!empty($differences)) {
        foreach ($differences as $fieldName => $fieldValue) {
          $user->set($fieldName, $fieldValue);
        }
        $user->save();
      }

      $user->set('field_access_token', $this->accessToken);
      $user->set('field_refresh_token', $this->refreshToken);
      $user->save();
      user_login_finalize($user);

    }
    else {

      $roleValue = $this->configFactory->get('azure_oauth_sso_roles_mapping.settings')->get('custom_attr_table');
      $roleOptions = $this->configFactory->get('azure_oauth_sso_roles_mapping.settings')->get('role_options');
      $defaultRole = $this->configFactory->get('azure_oauth_sso_roles_mapping.settings')->get('default_role');

      if ($roleOptions == 'ad_group') {
        $id = $this->getRolesPerGroup();
        $role = array_search($id, $roleValue);
        if ($role === FALSE) {
          $role = $defaultRole;
        }

      }
      elseif ($roleOptions == 'department_field') {

        $role = array_search($this->department, $roleValue);
        if ($role === FALSE) {
          $role = $defaultRole;
        }
      }
      else {
        $role = $defaultRole;
      }

      $fields = array_flip($this->configFactory->get('azure_oauth_sso_fields_mapping.settings')->get('custom_attr_table'));
      $fieldsKeys = array_keys($fields);

      $matchedValues = [];

      foreach ($fieldsKeys as $key) {
        if (array_key_exists($key, $apiRes)) {
          $matchedValues[$fields[$key]] = $apiRes[$key];
        }
      }

      $user = $user_storage->create($matchedValues);

      $user->addRole($role);
      $user->set("status", 1);
      $user->set("field_access_token", $this->accessToken);
      $user->set("field_refresh_token", $this->refreshToken);

      $user->save();

      user_login_finalize($user);
    }
  }

  /**
   * Retrieves personal data for the current user.
   */
  public function getPersonalData() {
    $fieldsMapping = array_values($this->configFactory->get('azure_oauth_sso_fields_mapping.settings')->get('custom_attr_table'));
    $fieldsMapping[] = 'displayName,department';

    $filteredValues = array_filter($fieldsMapping, function ($value) {
      return $value !== '_none' && !empty($value);
    });

    $urlApi = 'https://graph.microsoft.com/v1.0/me';

    if (!empty($filteredValues)) {
      $selectQueryString = '$select=' . implode(',', $filteredValues);

      $urlApi .= '?' . $selectQueryString;
    }

    $params = [
      'headers' => [
        'Content-Type'  => 'application/json',
        'Authorization' => $this->accessToken,
      ],
    ];

    $apiRes = $this->oauthService->apiCall($urlApi, $params, 'GET');

    $this->displayName = $apiRes['displayName'] ?? NULL;
    $this->department = $apiRes['department'] ?? NULL;
    return $apiRes;
  }

  /**
   * Retrieves roles per group for the current user.
   */
  public function getRolesPerGroup() {
    $urlApi = 'https://graph.microsoft.com/v1.0/me/transitiveMemberOf/microsoft.graph.group?$count=true';

    $params = [
      'headers' => [
        'Content-Type'  => 'application/json',
        'Authorization' => $this->accessToken,
      ],
    ];

    $apiRes = $this->oauthService->apiCall($urlApi, $params, 'GET');

    if (!empty($apiRes["value"])) {
      foreach ($apiRes["value"] as $data) {
        $id = $data["id"];
      }
    }

    return $id;
  }

  /**
   * Retrieves the photo of the current user.
   */
  public function getMyPhoto() {
    $token = $this->oauthService->getToken();
    $urlApi = 'https://graph.microsoft.com/v1.0/me/photo/$value';

    $params = [
      'headers' => [
        'Authorization' => 'Bearer ' . $token,
      ],
    ];

    $apiRes = $this->oauthService->apiCall($urlApi, $params, 'GET');

    if ($apiRes !== NULL) {
      $imageInfo = getimagesizefromstring($apiRes);

      if ($imageInfo !== FALSE) {
        $field_storage = FieldStorageConfig::loadByName('user', 'user_picture');

        $directory = $field_storage->getSetting('file_directory');
        if (empty($directory)) {
          $default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
          $directory = $this->configFactory->get('system.file')->get('default_' . $default_scheme . '_scheme') . 'public://';
        }

        if (!empty($directory) && substr($directory, -1) !== '/') {
          $directory .= '/';
        }

        $imageType = $imageInfo['mime'];
        $fileExtension = explode('/', $imageType);
        $fileName = $this->displayName . '.' . $fileExtension[1];

        $file_path = $directory . $fileName;

        if ($this->areImagesDifferent($file_path, $apiRes)) {
          file_put_contents($file_path, $apiRes);
          chmod($file_path, 0644);

          $file_entity = $this->saveFileToDatabase($file_path);
          $this->updateUserProfilePicture($file_entity);
        }
      }
      else {
        return NULL;
      }
    }
  }

  /**
   * Saves a file to the database.
   */
  private function saveFileToDatabase($filePath) {
    $file = File::create([
      'uri' => $filePath,
    ]);

    $file->save();

    return $file;
  }

  /**
   * Updates the user's profile picture.
   */
  private function updateUserProfilePicture(File $file) {
    try {
      $userId = $this->currentUser->id();
      $user = $this->entityTypeManager->getStorage('user')->load($userId);

      if ($user) {
        if ($user->hasField('user_picture') && !$user->get('user_picture')->isEmpty()) {
          $fid = $user->get('user_picture')->target_id;
          $oldFile = $this->entityTypeManager->getStorage('file')->load($fid);

          if ($oldFile) {
            $file->delete();
          }
        }

        $user->set('user_picture', $file->id());
        $user->save();
      }
    }
    catch (\Exception $e) {

    }
  }

  /**
   * Checks if two images are different.
   */
  private function areImagesDifferent($file_path, $newImageData) {
    try {
      $userId = $this->currentUser->id();
      $user = $this->entityTypeManager->getStorage('user')->load($userId);
      $entityId = $user->get('user_picture')->target_id;

      if ($entityId) {
        $file = $this->entityTypeManager->getStorage('file')->load($entityId);
        if ($file) {
          $fileContents = file_get_contents($file->getFileUri());
          if ($fileContents !== FALSE) {
            return md5($fileContents) !== md5($newImageData);
          }
          else {
            return TRUE;
          }
        }
        else {
          return TRUE;
        }
      }

      return TRUE;
    }
    catch (\Exception $e) {
      return TRUE;
    }
  }

}
