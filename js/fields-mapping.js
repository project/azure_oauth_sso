/**
 * @file
 * Custom JavaScript behaviors for the attribute mapping module in Drupal.
 *
 * This script manages the state of form elements within the attribute mapping
 * table. Specifically, it handles the enabling and disabling of select elements
 * based on the state of associated checkboxes.
 *
 * - Disables all select elements by default on page load.
 * - Adds event listeners to checkboxes to toggle the disabled state of the
 *   corresponding select elements.
 * - Ensures that the select elements are enabled or disabled appropriately
 *   based on the initial state of the checkboxes.
 */

(function ($) {
    Drupal.behaviors.attributeMapping = {
      attach: function (context, settings) {

        const table = document.getElementById("attribute-mapping");
        const checkboxes = table.getElementsByClassName("custom-attribute-checkbox");
        const texts = table.getElementsByClassName("form-select");

        for (let i = 0; i < texts.length; i++) {
          texts[i].disabled = true;
        }

        function toggleTextDisabledState() {
          for (let i = 0; i < checkboxes.length; i++) {
            texts[i].disabled = !checkboxes[i].checked;
          }
        }

        for (let i = 0; i < checkboxes.length; i++) {
          checkboxes[i].addEventListener("change", toggleTextDisabledState);
        }

        toggleTextDisabledState();
      },
    };
  })(jQuery);
