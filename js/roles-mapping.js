/**
 * @file
 * Custom JavaScript behaviors for the attribute mapping module in Drupal.
 *
 * This script manages the state of form elements within the attribute mapping
 * table and updates placeholders based on selected options.
 *
 * Functionality:
 * - Disables all text input fields by default on page load.
 * - Adds event listeners to checkboxes to toggle the disabled state of the
 *   corresponding text input fields.
 * - Ensures that the text input fields are enabled or disabled appropriately
 *   based on the initial state of the checkboxes.
 * - Updates placeholders of text input fields based on the selected radio button
 *   option. The placeholder changes to provide context-specific guidance for
 *   entering data.
 * - Adds event listeners to radio buttons to dynamically update placeholders
 *   when the selected option changes.
 */

(function ($) {
    Drupal.behaviors.attributeMapping = {
      attach: function (context, settings) {
        const table = document.getElementById("attribute-mapping");
        const checkboxes = table.getElementsByClassName("custom-attribute-checkbox");
        const texts = table.getElementsByClassName("form-text");

        for (let i = 0; i < texts.length; i++) {
          texts[i].disabled = true;
        }

        function toggleTextDisabledState() {
          for (let i = 0; i < checkboxes.length; i++) {
            texts[i].disabled = !checkboxes[i].checked;
          }
        }

        for (let i = 0; i < checkboxes.length; i++) {
          checkboxes[i].addEventListener("change", toggleTextDisabledState);
        }

        toggleTextDisabledState();

        function updatePlaceholders() {
          var selectedValue = document.querySelector('input[name="role_options"]:checked').value;
          var formTextInputs = document.querySelectorAll('.js-form-type-textfield .form-text');

          formTextInputs.forEach(function (textInput) {
            if (selectedValue === 'ad_group') {
              textInput.placeholder = 'Enter the (Object Id) from Azure AD';
            } else if (selectedValue === 'department_field') {
              textInput.placeholder = 'Enter a department name';
            }
          });
        }

        updatePlaceholders();
        var radioElements = document.querySelectorAll('.form-boolean-group input[type="radio"]');
        radioElements.forEach(function (radio) {
          radio.addEventListener('change', updatePlaceholders);
        });
      },
    };
  })(jQuery);
