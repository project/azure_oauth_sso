/**
 * @file
 * Custom JavaScript behaviors for the attribute mapping module in Drupal.
 *
 * This script defines a set of behaviors for handling click events related to
 * copying text to the clipboard and interacting with OAuth configuration testing.
 *
 * - Adds a click event listener to elements with the class "mo_copy" to copy
 *   the text of the preceding element to the clipboard.
 * - Manages the "selected-text" class for visual feedback when text is copied.
 * - Handles click events outside of the "mo_copy" elements to remove the
 *   "selected-text" class.
 * - Provides a function to open a new window for testing OAuth configuration.
 */

(function ($) {
  Drupal.behaviors.attributeMapping = {
    attach: function (context, settings) {
      jQuery(document).ready(function ($) {
        $(".mo_copy").click(function () {
          copyToClipboard('#' + $(this).prev().attr("id"));
        });
      });

      function copyToClipboard(element) {
        jQuery(".selected-text").removeClass("selected-text");
        var temp = jQuery("<input>");
        jQuery("body").append(temp);
        jQuery(element).addClass("selected-text");
        temp.val(jQuery(element).text().trim()).select();
        document.execCommand("copy");
        temp.remove();
      }

      function handleClick(e) {
        if (e.target.className == undefined || e.target.className.indexOf("mo_copy") == -1) {
            jQuery(".selected-text").removeClass("selected-text");
        }
      }

      jQuery(window).click(handleClick);

      window.testconfig = function (testUrl) {
        var testWindow = window.open(testUrl, "Test OAuth config", "scrollbars=1,width=800,height=600");
      };
    },
  };
})(jQuery);
